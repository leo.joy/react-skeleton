import { HomePage } from '../pages/home';

export const RouteData = [
  {
    path: '/',
    component: HomePage,
    showSideBar: true,
    showRouteInSideBar: true,
    title: 'Home',
    order: 1,
    id: 1,
  },
];
